package postgres

import "github.com/jmoiron/sqlx"

type bookRepo struct{
	Db 	*sqlx.DB
}

func NewBookRepo(db *sqlx.DB) *bookRepo{
	return &bookRepo{Db: db}
}