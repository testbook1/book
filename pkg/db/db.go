package db

import (
	"fmt"
	"temp/config"
	_ "github.com/lib/pq" // postgres driver
	"github.com/jmoiron/sqlx"
)

func ConnectToDB(cfg config.Config) (*sqlx.DB, error){
	psqlString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
	cfg.PostgresHost,
	cfg.PostgresPort,
	cfg.PostgresUser,
	cfg.PostgresPassword,
	cfg.PostgresDatabase,
)
// return sqlx.Connect("postgres",psqlString)
fmt.Println(psqlString)
connDb, err := sqlx.Connect("postgres",psqlString)
if err != nil{
	return nil, err
}

return connDb, nil
}

func ConnectToDBForSuite(cfg config.Config) (*sqlx.DB, func()) {
	psqlString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
	cfg.PostgresHost,
	cfg.PostgresPort,
	cfg.PostgresUser,
	cfg.PostgresPassword,
	cfg.PostgresDatabase,
)
connDb, err := sqlx.Connect("postgres", psqlString)
if err != nil{
	return nil, func() {}
}
cleanpFunc := func ()  {
	connDb.Close()
}

return connDb, cleanpFunc
}